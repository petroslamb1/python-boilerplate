# Python Boilerplate 

Python Boilerplate contains all the boilerplate you need to create a Python package.

 * Free software: MIT license * Documentation: https://gitlab.com/path-to-your-project


# Features

-   TODO

# Credits

This package was created with
[Cookiecutter](https://github.com/audreyr/cookiecutter) and the
[open-source/toolbox/cookiecutter-pypackage](https://gitlab.com/petroslamb1/gitlab-cookiecutter-pypackage)
project template.
