* Python Boilerplate version:


### Description

Describe shortly what the new feature is about.
Tell us what you would like to achieve.

### Use cases

Give an illustration of a typical use case for the new feature.
Describe the problem that needs to be solved, the actors involved and the desired outcome.
